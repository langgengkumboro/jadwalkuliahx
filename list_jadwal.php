			<a href="index.php?page=input_jadwal" class="btn btn-primary btn-sm active mt-3 mb-3" aria-pressed="true">
            	Add New
            </a>
            <table class="table table-hover table-bordered">
            	<thead class="thead-dark">
                	<tr>
                        <th scope="col">Kode Jadwal</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Jurusan</th>
                        <th scope="col">Matakuliah</th>
                        <th scope="col">SKS</th>
                        <th scope="col">Ruangan</th>
                        <th scope="col">Letak</th>
                        <th scope="col">Dosen</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
			<?php
            $qr_jadwal=mysqli_query($cn, "SELECT * FROM jadwal as jd 
			INNER JOIN kelas as kl ON jd.kode_kelas=kl.kode_kelas 
			INNER JOIN ruangan as rk ON jd.kode_ruangan=rk.kode_ruangan 
			INNER JOIN dosen as ds ON jd.kode_dosen=ds.kode_dosen 
			ORDER BY kode_jadwal ASC");
			while($f_jadwal=mysqli_fetch_array($qr_jadwal)){
			?>
                	<tr>
                    	<td><?=$f_jadwal['kode_jadwal'];?></td>
                    	<td><?=$f_jadwal['tanggal'];?></td>
                    	<td><?=$f_jadwal['kelas'];?></td>
                    	<td><?=$f_jadwal['jurusan'];?></td>
                    	<td><?=$f_jadwal['mata_kuliah'];?></td>
                    	<td><?=$f_jadwal['sks'];?></td>
                    	<td><?=$f_jadwal['nama_ruangan'];?></td>
                    	<td><?=$f_jadwal['letak'];?></td>
                    	<td><?=$f_jadwal['nama'];?></td>
                    	<td>
		<a href="index.php?page=edit_jadwal&kode=<?=$f_jadwal['kode_jadwal'];?>">Edit</a>
		<a href="delete_jadwal.php?kode=<?=$f_jadwal['kode_jadwal'];?>">
                         | Del
                         </a>
                         </td>
                    </tr>
			<?php
			}
			?>
                </tbody>
            </table>